**Grimoire is a Mod for Minecraft**

This is the master source for Minecraft Version 1.12.2
*Please note that the source for Minecraft Versions 1.14 and 1.15 will be uploaded upon public release*

If you have any suggestions for Grimoire, please post as such in the discord server dedicated to Grimoire's Development! 
Discord Server Invite Link: https://discordapp.com/invite/dSYtJ5m

The sourse code is open for anyone to use, if you can sift through the tangled mess to find something usefull, by all means use it how you wish!