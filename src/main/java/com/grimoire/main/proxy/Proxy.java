package com.grimoire.main.proxy;

import net.minecraftforge.fml.event.lifecycle.*;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.event.server.FMLServerStoppedEvent;

public interface Proxy {
    /**
     * First mod registration event. Called to
     * initialize and register the Resynth mod.
     *
     * @param event forge provided event.
     */
    void onSetup(final FMLCommonSetupEvent event);

    default void onFinishSetup(final FMLLoadCompleteEvent event){}

    // Optional

    /**
     * Client side mod registration event.
     *
     * @param event forge provided event.
     */
    default void onClientSetup(final FMLClientSetupEvent event){}

    /**
     * InterModCommunication message send event.
     *
     * @param event forge provided event.
     */
    @SuppressWarnings({"unused", "RedundantSuppression"})
    default void onEnqueueModComs(final InterModEnqueueEvent event){}

    /**
     * InterModCommunication process event.
     *
     * @param event forge provided event.
     */
    @SuppressWarnings({"unused", "RedundantSuppression"})
    default void onProcessModComs(final InterModProcessEvent event){}

    /**
     * Server side registration event.
     *
     * @param event forge provided event.
     */
    @SuppressWarnings({"unused", "RedundantSuppression"})
    default void onServerStarting(FMLServerStartingEvent event){}

    /**
     * Called when the game is stopping.
     *
     * @param event Forge event.
     */
    @SuppressWarnings({"unused", "RedundantSuppression"})
    default void onServerStopped(FMLServerStoppedEvent event){}
}
