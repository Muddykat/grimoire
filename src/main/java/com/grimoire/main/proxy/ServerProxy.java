package com.grimoire.main.proxy;

import com.grimoire.main.Grimoire;
import com.grimoire.main.registry.FeatureRegistry;
import net.minecraftforge.fml.event.lifecycle.*;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.event.server.FMLServerStoppedEvent;
import org.apache.logging.log4j.Logger;

public class ServerProxy implements Proxy{

    private static final Logger LOGGER = Grimoire.getNewLogger();

    public ServerProxy(){
        LOGGER.info("Grimoire ServerProxy Initialized!");
    }

    @Override
    public void onSetup(FMLCommonSetupEvent event) {
        event.enqueueWork(() -> {
            FeatureRegistry.init();
            FeatureRegistry.registerFeatures();
        });
    }

    @Override
    public void onServerStarting(FMLServerStartingEvent event) {

    }

    @Override
    public void onServerStopped(FMLServerStoppedEvent event) {

    }
}
