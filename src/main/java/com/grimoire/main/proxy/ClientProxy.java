package com.grimoire.main.proxy;

import com.grimoire.main.client.menu.handler.CreativeMenuHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.event.lifecycle.*;

public class ClientProxy extends ServerProxy {
    @Override
    public void onSetup(FMLCommonSetupEvent event) {
        super.onSetup(event);
    }

    @Override
    public void onClientSetup(FMLClientSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new CreativeMenuHandler());
    }
}
