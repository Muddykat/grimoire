package com.grimoire.main.registry;

import java.util.ArrayList;

import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.common.item.ItemBase;
import com.grimoire.main.common.item.ItemGuideGrimoire;

import net.minecraft.item.Item;
import net.minecraft.item.Item.Properties;
import net.minecraftforge.event.RegistryEvent.Register;

public class ItemRegistry {

	public static ArrayList<Item> items = new ArrayList<Item>();

	public static Item GRIMOIRE, MOON_DUST, INKWELL, ESSENCE_METER, 
						MOONSTONE, RESEARCH_PAGE, RUNESTONE_SWORD, RUNESTONE_SPADE,
						RUNESTONE_PICKAXE, RUNESTONE_AXE, RUNESTONE_HOE, WARD_RUNE,
						STYGIAN_INGOT, NETHER_DUST;

	public static Properties defaultProperties = new Properties();

	public static void intializeItem() {
		defaultProperties.tab(ItemGroupRegistry.GRIMOIRE);
		// Runology blocks
		items.add(GRIMOIRE = new ItemGuideGrimoire(defaultProperties, "item_grimoire", ItemSubGroup.runeology));
		items.add(MOON_DUST = new ItemBase(defaultProperties.stacksTo(64), "moon_dust",ItemSubGroup.runeology));
		items.add(INKWELL = new ItemBase(defaultProperties, "item_inkwell",ItemSubGroup.runeology));
		items.add(ESSENCE_METER = new ItemBase(defaultProperties, "essence_meter",ItemSubGroup.runeology));
		items.add(MOONSTONE = new ItemBase(defaultProperties, "moonstone",ItemSubGroup.sorcery));
		items.add(STYGIAN_INGOT = new ItemBase(defaultProperties, "stygian", ItemSubGroup.demonology));
	}

	public static void parseItemRegistry(Register<Item> event) {
		intializeItem();
		for (Item i : items) {
			event.getRegistry().register(i);
		}
	}

}
