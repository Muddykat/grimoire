package com.grimoire.main.registry.helper;

import net.minecraft.item.Item;

public interface IBlock {
	public Item getItemBlock();
}
