package com.grimoire.main.registry.helper;

import com.grimoire.main.Grimoire;
import com.grimoire.main.lib.GrimoireLib;
import com.grimoire.main.registry.FeatureRegistry;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber(modid = GrimoireLib.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ForgeEventSubscriber {
    private static final Logger LOGGER = Grimoire.getNewLogger();

    @SubscribeEvent(priority= EventPriority.HIGH)
    public static void onBiomeLoading(BiomeLoadingEvent evt)
    {
        FeatureRegistry.onBiomeLoading(evt);
    }

}
