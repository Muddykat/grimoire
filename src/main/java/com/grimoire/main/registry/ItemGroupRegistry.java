package com.grimoire.main.registry;

import com.grimoire.main.client.menu.GrimoireItemGroup;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ItemGroupRegistry {
   public static final GrimoireItemGroup GRIMOIRE = new GrimoireItemGroup("grimoire");
}
