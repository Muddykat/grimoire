package com.grimoire.main.registry;

import com.grimoire.main.Grimoire;
import com.grimoire.main.common.feature.GrimoireFeature;
import com.grimoire.main.common.feature.GrimoireOreFeature;
import com.grimoire.main.common.feature.helper.MatchBlockListRuleTest;
import com.grimoire.main.config.GrimoireConfig;
import com.grimoire.main.config.NetherOreGenConfig;
import com.grimoire.main.config.OreGenConfig;
import com.grimoire.main.config.OverworldOreGenConfig;
import com.grimoire.main.lib.GrimoireLib;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FeatureRegistry {
    private static final Logger LOG = Grimoire.getNewLogger();

    private static final List<GrimoireFeature<?>> FEATURE_LIST = new ArrayList<>();

    private static final NetherOreGenConfig NETHER_ORE_GEN_CONFIG = GrimoireConfig.GENERAL_CONFIG.getCategory(NetherOreGenConfig.class);
    private static final OverworldOreGenConfig OVERWORLD_ORE_GEN_CONFIG = GrimoireConfig.GENERAL_CONFIG.getCategory(OverworldOreGenConfig.class);


    private static GrimoireFeature<GrimoireOreFeature> STYGIAN_IRON_ORE = new GrimoireOreFeature(
            new ResourceLocation(GrimoireLib.MODID,  "ore_gen_stygian"), getNetherworldBiomes(),
            BlockRegistry.STYGIAN_ORE, MatchBlockListRuleTest.MATCH_NETHERWORLD_ROCK,
            NETHER_ORE_GEN_CONFIG.getRarity(), NETHER_ORE_GEN_CONFIG.getSize(),
            NETHER_ORE_GEN_CONFIG.getMinimumHeight(), NETHER_ORE_GEN_CONFIG.getMaximumHeight()
    ).register();

    private static GrimoireFeature<GrimoireOreFeature> NAT_RUNESTONE = new GrimoireOreFeature(
            new ResourceLocation(GrimoireLib.MODID,  "ore_gen_runestone"), getOverworldBiomes(),
            BlockRegistry.RUNESTONE, MatchBlockListRuleTest.MATCH_OVERWORLD_ROCK,
            OVERWORLD_ORE_GEN_CONFIG.getRarity(), OVERWORLD_ORE_GEN_CONFIG.getSize(),
            OVERWORLD_ORE_GEN_CONFIG.getMinimumHeight(), OVERWORLD_ORE_GEN_CONFIG.getMaximumHeight()
    ).register();

    public static <T extends GrimoireFeature<?>> T addFeature(T feature) {
        FEATURE_LIST.add(Objects.requireNonNull(feature));
        return feature;
    }


    public static void onBiomeLoading(BiomeLoadingEvent event) {
        BiomeGenerationSettingsBuilder generation = event.getGeneration();
        FEATURE_LIST.forEach((grimoireFeature -> grimoireFeature.configure(generation, event.getCategory())));
    }

    public static void registerFeatures() {
        FEATURE_LIST.stream().peek(
                feature -> LOG.info("Registering GrimoireFeature {}.", feature.getID().toString())
        ).filter(
                feature -> feature.getFeature() != null
        ).forEach(
                feature -> Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, feature.getID(), feature.getFeature())
        );
    }

    private static Biome.Category[] getNetherworldBiomes() {
        return new Biome.Category[]{ Biome.Category.NETHER };
    }

    private static Biome.Category[] getOverworldBiomes(){
        return new Biome.Category[]{
            Biome.Category.TAIGA, Biome.Category.EXTREME_HILLS, Biome.Category.JUNGLE, Biome.Category.MESA,
            Biome.Category.PLAINS, Biome.Category.SAVANNA, Biome.Category.ICY, Biome.Category.BEACH, Biome.Category.FOREST,
            Biome.Category.OCEAN, Biome.Category.DESERT, Biome.Category.RIVER, Biome.Category.SWAMP, Biome.Category.MUSHROOM
        };
    }

    public static void init() {    }
}
