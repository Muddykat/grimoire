package com.grimoire.main.registry;

import java.util.ArrayList;

import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.common.block.BlockBase;
import com.grimoire.main.common.block.BlockModelBase;
import com.grimoire.main.common.block.helper.IModeledBlock;
import com.grimoire.main.registry.helper.IBlock;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent.Register;

public class BlockRegistry {

	public static ArrayList<Block> blocks = new ArrayList<Block>();

	public static Block MOONSTONE_ALTAR, WARD_MATRIX, WARD_FOCI, HELLFORGE, RESEARCH_TABLE;
	public static Block RUNESTONE_TILES, RUNESTONE, SMOOTH_RUNESTONE, INSCRIBED_SMOOTH_RUNESTONE;
	public static Block STYGIAN_IRONBLOCK, STYGIAN_ORE, RUNESTONE_PILLAR;

	public static AbstractBlock.Properties defaultProperties = AbstractBlock.Properties.of(Material.STONE).strength(2F, 1F);
	public static AbstractBlock.Properties netherProperties = AbstractBlock.Properties.of(Material.STONE, MaterialColor.NETHER).requiresCorrectToolForDrops().strength(0.6F).sound(SoundType.NETHERRACK);

	public static void intializeBlocks() {
		//Runology blocks
		blocks.add(RUNESTONE_TILES = new BlockModelBase(defaultProperties, "runestone_tiles", ItemSubGroup.runeology));
		blocks.add(RUNESTONE = new BlockModelBase(defaultProperties, "runestone", ItemSubGroup.runeology));
		blocks.add(SMOOTH_RUNESTONE = new BlockModelBase(defaultProperties, "smooth_runestone", ItemSubGroup.runeology));
		blocks.add(INSCRIBED_SMOOTH_RUNESTONE = new BlockModelBase(defaultProperties, "inscribed_smooth_runestone", ItemSubGroup.runeology));
		blocks.add(MOONSTONE_ALTAR = new BlockModelBase(defaultProperties, "moonstone_altar", ItemSubGroup.runeology));
		blocks.add(WARD_MATRIX = new BlockModelBase(defaultProperties, "ward_matrix", ItemSubGroup.runeology));
		blocks.add(WARD_FOCI = new BlockModelBase(defaultProperties, "runic_ward_focus", ItemSubGroup.runeology));
		blocks.add(STYGIAN_IRONBLOCK = new BlockModelBase(defaultProperties, "stygian_block", ItemSubGroup.demonology));
		blocks.add(STYGIAN_ORE = new BlockModelBase(netherProperties, "netherrack_stygian", ItemSubGroup.demonology));
		blocks.add(HELLFORGE = new BlockModelBase(defaultProperties, "hellforge", ItemSubGroup.demonology));
		blocks.add(RESEARCH_TABLE = new BlockModelBase(defaultProperties, "research_table", ItemSubGroup.sorcery));
	}

	public static void parseBlockRegistry(Register<Block> event) {
		intializeBlocks();
		for (Block b : blocks) {
			event.getRegistry().register(b);
		}
	}

	public static void parseBlockItemRegistry(Register<Item> event) {
		for (Block b : blocks) {
			if (b instanceof IBlock) {
				event.getRegistry().register(((IBlock) b).getItemBlock()); // change to a GrimoireSubGroup implemented itemblock
			}
		}
	}
}
