package com.grimoire.main.client.menu.handler;

import java.util.ArrayList;
import java.util.List;

import com.grimoire.main.client.menu.GrimoireItemGroup;
import com.grimoire.main.client.menu.helper.GrimoireSubGroup;
import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.lib.GrimoireLib;
import com.grimoire.main.registry.ItemGroupRegistry;
import com.grimoire.main.registry.ItemRegistry;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.inventory.CreativeScreen;
import net.minecraft.client.gui.screen.inventory.CreativeScreen.CreativeContainer;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.gui.GuiUtils;

public class CreativeMenuHandler {
	private static final ResourceLocation CEX_GUI_TEXTURES = new ResourceLocation(GrimoireLib.MODID,"textures/gui/creative_expansion.png");
	private static ArrayList<CreativeMenuButton> subGroupButtons = new ArrayList<CreativeMenuButton>();

	@SubscribeEvent
	public void drawScreen(GuiScreenEvent.BackgroundDrawnEvent event) {
		Screen screen = event.getGui();
		if(screen instanceof CreativeScreen) {
			CreativeScreen gui = (CreativeScreen) screen; 
			int i = (int) (gui.getGuiLeft() - Math.floor(136*1.425));
			
			if(gui.getSelectedTab() == ItemGroupRegistry.GRIMOIRE.getId()) {
				if(!subGroupButtons.isEmpty()) {
					subGroupButtons.forEach((button) -> {
						button.active = true;
						button.visible = true;
					});
				}

				MatrixStack matrixStack = event.getMatrixStack();
				matrixStack.pushPose();
				Minecraft.getInstance().textureManager.bind(CEX_GUI_TEXTURES);
				GlStateManager._clearColor(1F, 1F, 1F, 1F);

				AbstractGui.blit(matrixStack, i + 166, gui.getGuiTop(), 0, 0, 29, 136, 256, 256);
				matrixStack.popPose();
			} else {
				if(!subGroupButtons.isEmpty()) {
					subGroupButtons.forEach((button) -> {
						button.active = false;
						button.visible = false;	
					});
				}
			}
		}
	}
	
	@SubscribeEvent
    public void initializeGuiEvent(GuiScreenEvent.InitGuiEvent event) {
		Screen screen = event.getGui();
		if(screen instanceof CreativeScreen) {
			CreativeScreen gui = (CreativeScreen) screen;
			int i = (int) (gui.getGuiLeft() - Math.floor(136*1.425));
			int j = (gui.height - 195) / 2; 

			for(int iteration = 0; iteration < ItemSubGroup.values().length; iteration++) {
						ItemSubGroup currentGroup = ItemSubGroup.values()[iteration];
				
						CreativeMenuButton button = new CreativeMenuButton(gui, currentGroup, i + 166 + 7, j + 46 + (23 * iteration),button1 -> {
						GrimoireItemGroup.updateSubGroup(currentGroup);
						gui.getMenu().setAll(new ArrayList<ItemStack>());
						PlayerInventory playerinventory = gui.getMinecraft().player.inventory;
						gui.getMenu().slots.forEach((slot) -> {
							if(slot.container != playerinventory){
								slot.set(ItemStack.EMPTY);
								slot.setChanged();
							}
						});

						ItemGroupRegistry.GRIMOIRE.fillItemList(gui.getMenu().getItems());

						int slotIteration = 0;
						for(int l = 0; l < gui.getMenu().slots.size(); l++) {
							Slot slot = gui.getMenu().slots.get(l);
							if(slot.container != playerinventory){
								if(slotIteration < gui.getMenu().items.size()) {
									slot.set(gui.getMenu().items.get(slotIteration));
									slot.setChanged();
									slotIteration++;
								}
							}
						}
						//resize the gui to the same size, quick way to get it to update the content
						gui.resize(gui.getMinecraft(), gui.width, gui.height);
					});
				
				subGroupButtons.add(button);
				event.addWidget(button);	
			}
		}
	}
	
	public class CreativeMenuButton extends Button {

		public CreativeScreen screen;
		public CreativeContainer contained;
		public ItemSubGroup group;
		public CreativeMenuButton(CreativeScreen screen, ItemSubGroup group, int x, int y, IPressable onPress) {
			super(18,18,x, y, new StringTextComponent("Tooltip?"), onPress);
			this.width = 18;
			this.height = 18;
			this.x = x;
			this.y = y;
			this.contained = screen.getMenu();
			this.screen = screen;
			this.group = group;
		}

		@Override
		public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float ticks) {
			Minecraft mc = Minecraft.getInstance();
			Minecraft.getInstance().textureManager.bind(CEX_GUI_TEXTURES);
			GlStateManager._clearColor(1F, 1F, 1F, 1F);
			RenderHelper.setupForFlatItems();
			boolean hovered = mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height;
			
			AbstractGui.blit(matrixStack, x, y, ((hovered || (GrimoireItemGroup.getCurrentSubGroup().equals(group))) ? 29 : 47), 0, width, height, 256, 256);
			
			ItemStack stack = new ItemStack(Items.ROTTEN_FLESH);

			if(group.ordinal() == 0)
				stack = new ItemStack(ItemRegistry.MOON_DUST);
			if(group.ordinal() == 1)
				stack = new ItemStack(ItemRegistry.STYGIAN_INGOT);
			if(group.ordinal() == 2)
				stack = new ItemStack(ItemRegistry.INKWELL);



			RenderHelper.setupFor3DItems();
			if(hovered || (GrimoireItemGroup.getCurrentSubGroup().equals(group))) {
				mc.getItemRenderer().renderGuiItem(stack, x + 1, y + 2);
			} else {
				mc.getItemRenderer().renderGuiItem(stack, x + 1, y + 1);
			}
			
			//Tool tip on hover
			if(hovered) {
				String name = group.name();
				String name_part = name.substring(0, 1).toUpperCase();
				String corrected_name = name_part + name.substring(1);
				List<IFormattableTextComponent> tooltip = new ArrayList<>();
				tooltip.add(new StringTextComponent(corrected_name));


				//we divide the width by four to force the tooltip to the left, as it's on a lower layer then the items in the creative menu.
				GuiUtils.drawHoveringText(matrixStack, tooltip,mouseX,mouseY,(mc.screen.width / 4),mc.screen.height,80,mc.font);
			}
		}
	}
}

