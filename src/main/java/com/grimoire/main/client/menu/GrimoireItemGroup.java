package com.grimoire.main.client.menu;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.grimoire.main.client.menu.helper.GrimoireSubGroup;
import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.registry.ItemGroupRegistry;
import com.grimoire.main.registry.ItemRegistry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraft.util.NonNullList;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class GrimoireItemGroup extends ItemGroup {
	
	public static ItemSubGroup selectedSubGroup = ItemSubGroup.runeology;
	
	public GrimoireItemGroup(String label) {
		super(label);
	}

	@Override
	public ItemStack makeIcon() {
		return new ItemStack(ItemRegistry.GRIMOIRE);
	}

	public static void updateSubGroup(ItemSubGroup group) {
		selectedSubGroup = group;
	}
	
	public static ItemSubGroup getCurrentSubGroup() {
		return selectedSubGroup;
	}

	/**
    * Fills {@code items} with all items that are in this group.
    */
	@Override
    @OnlyIn(Dist.CLIENT)
    public void fillItemList(NonNullList<ItemStack> items) {
		for(Item item : Registry.ITEM) {
			if(item instanceof GrimoireSubGroup) {
				GrimoireSubGroup itm = (GrimoireSubGroup)item;
				if(itm.getSubGroup() == selectedSubGroup) {
					item.fillItemCategory(this, items);
				}
			}
		}
    }

	public static NonNullList<ItemStack> getCurrentList() {
		NonNullList<ItemStack> list = NonNullList.create();
		for(Item item : Registry.ITEM) {
			if(item instanceof GrimoireSubGroup) {
				GrimoireSubGroup itm = (GrimoireSubGroup)item;
				if(itm.getSubGroup() == selectedSubGroup) {
					item.fillItemCategory(ItemGroupRegistry.GRIMOIRE, list);
				}
			}
		}
		return list;
	}

}

