package com.grimoire.main.client.menu.helper;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import com.grimoire.main.registry.ItemRegistry;

public enum ItemSubGroup {
	runeology(ItemRegistry.MOON_DUST),
	demonology(ItemRegistry.STYGIAN_INGOT),
	sorcery(ItemRegistry.MOONSTONE);
	
	private Item icon;
	
	ItemSubGroup(Item stack) {
		this.icon = stack;
	}
	 
	public Item getIcon() {
		return icon;
	}
}
