package com.grimoire.main.client.menu.helper;

public interface GrimoireSubGroup {
	public ItemSubGroup getSubGroup();
}
