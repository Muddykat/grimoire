package com.grimoire.main.client.gui;

import com.grimoire.main.client.menu.GrimoireItemGroup;
import com.grimoire.main.lib.GrimoireLib;
import com.grimoire.main.registry.ItemRegistry;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.ReadBookScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

public class GrimoireScreen extends Screen {
    private static final ResourceLocation GRIMOIRE_GUI_TEXTURES = new ResourceLocation(GrimoireLib.MODID,"textures/gui/block_frame.png");

    public GrimoireScreen(ITextComponent p_i51108_1_) {
        super(p_i51108_1_);
        this.init(Minecraft.getInstance(), 256,256);
        this.buttons.add(new GrimoireNodeButton(120,120, onPress -> {

        }));
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    @Override
    public void render(MatrixStack matrix, int mx, int my, float ticks) {
        super.render(matrix, mx, my, ticks);
        this.buttons.forEach(button -> button.render(matrix, mx, my, ticks));
    }

    public class GrimoireNodeButton extends Button {

        public GrimoireNodeButton(int x, int y, IPressable onPress){
            super(26,26,x,y,new StringTextComponent("Tooltip?"), onPress);
            this.width = 26;
            this.height = 26;
            this.x = x;
            this.y = y;
        }

        @Override
        public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float ticks) {
            Minecraft mc = Minecraft.getInstance();
            matrixStack.pushPose();
            Minecraft.getInstance().textureManager.bind(GRIMOIRE_GUI_TEXTURES);
            GlStateManager._clearColor(1F, 1F, 1F, 1F);
            RenderHelper.setupForFlatItems();
            boolean hovered = mouseX >= x && mouseY >= y && mouseX < x + width && mouseY < y + height;
            AbstractGui.blit(matrixStack, x, y, 0, 0, width, height, 256, 256);
            matrixStack.popPose();
            mc.getItemRenderer().renderGuiItem(new ItemStack(ItemRegistry.GRIMOIRE), x, y);
        }
    }
}
