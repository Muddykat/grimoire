package com.grimoire.main.config;

import com.grimoire.main.config.helpers.ConfigFile;

import java.io.File;

public class GrimoireConfig {
    private static final String CONFIG_FOLDER = "config/grimoire/";
    static {
        //Ensure parent folders exist.
        File configFolder = new File(CONFIG_FOLDER);
        if(!configFolder.exists())
            configFolder.mkdirs();
    }

    public static final ConfigFile GENERAL_CONFIG = new ConfigFile(newConfig("general"));

    private GrimoireConfig(){}

    @SuppressWarnings("SameParameterValue")
    private static String newConfig(String name){
        return CONFIG_FOLDER + name + ".toml";
    }
}
