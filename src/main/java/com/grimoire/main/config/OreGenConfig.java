package com.grimoire.main.config;

public class OreGenConfig extends GenConfig {

    public OreGenConfig(){
        super("ore-gen", true, 12, 1, 1, 254);
    }

}
