package com.grimoire.main.config;

public class NetherOreGenConfig extends GenConfig {

    public NetherOreGenConfig(){
        super("nether_ore-gen", true, 12, 1, 1, 254);
    }

}
