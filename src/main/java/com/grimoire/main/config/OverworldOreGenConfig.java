package com.grimoire.main.config;

public class OverworldOreGenConfig extends GenConfig {

    public OverworldOreGenConfig(){
        super("overworld_ore-gen", true, 64, 1, 1, 254);
    }

}
