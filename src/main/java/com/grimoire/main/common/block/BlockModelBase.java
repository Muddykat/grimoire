package com.grimoire.main.common.block;

import com.grimoire.main.client.menu.helper.ItemSubGroup;

public class BlockModelBase extends BlockBase {

	public BlockModelBase(Properties properties, String registryName, ItemSubGroup subGroup) {
		super(properties, registryName, subGroup);
		properties.dynamicShape();
		properties.noOcclusion();
	}

}
