package com.grimoire.main.common.block.helper;

import com.grimoire.main.client.menu.helper.GrimoireSubGroup;
import com.grimoire.main.client.menu.helper.ItemSubGroup;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;

public class GrimoireBlockItem extends BlockItem implements GrimoireSubGroup{

	private ItemSubGroup subGroup;
	
	public GrimoireBlockItem(Block blockIn, Properties builder, ItemSubGroup subGroup) {
		super(blockIn, builder);
		this.subGroup = subGroup;
	}

	@Override
	public ItemSubGroup getSubGroup() {
		// TODO Auto-generated method stub
		return subGroup;
	}
 
}
