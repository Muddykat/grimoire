package com.grimoire.main.common.block;


import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.common.block.helper.GrimoireBlockItem;
import com.grimoire.main.registry.ItemGroupRegistry;
import com.grimoire.main.registry.helper.IBlock;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public class BlockBase extends Block implements IBlock {

	public Item itemBlock = null;
	public Item drop = null;
	
	private boolean hasSubTypes = false;
	private boolean isRunic = false;
	private boolean isDemonic = false;
	
	public BlockBase(Properties properties, String registryName, ItemSubGroup subGroup) {
		super(properties);
		setRegistryName("grimoire", registryName);
		itemBlock = new GrimoireBlockItem(this, new net.minecraft.item.Item.Properties().tab(ItemGroupRegistry.GRIMOIRE),subGroup).setRegistryName(getRegistryName());
		properties.strength(2F, 1F);
	}
	
	@Override
	public Item getItemBlock() {
		// TODO Auto-generated method stub
		return itemBlock;
	}
}
