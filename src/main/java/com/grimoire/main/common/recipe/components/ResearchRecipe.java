package com.grimoire.main.common.recipe.components;

import net.minecraft.item.ItemStack;

public class ResearchRecipe {
	public ItemStack input = null;
	public ItemStack output = null;
	
	public ResearchRecipe(ItemStack input, ItemStack output){
		this.input = input;
		this.output = output;
	}
	
	public ItemStack getInput(){
		return input;
	}
	
	public ItemStack getOutput(){
		return output;
	}
}
