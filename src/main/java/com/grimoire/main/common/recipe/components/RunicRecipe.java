package com.grimoire.main.common.recipe.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.grimoire.main.common.recipe.util.ItemUtils;

import net.minecraft.item.ItemStack;

public class RunicRecipe {
	Random random = new Random();
	public List<ItemStack> pillerInputs = new ArrayList<ItemStack>();
	public ItemStack centerInput = ItemStack.EMPTY;
	
	public ItemStack result = ItemStack.EMPTY;
	private int resultmeta = 0;
	
	public RunicRecipe(ItemStack center, ItemStack northPiller, ItemStack southPiller, ItemStack eastPiller, ItemStack westPiller, ItemStack result){
		centerInput = center;
		this.result = result;
		resultmeta = result.getDamageValue();//may need to change to... TAGS. ugh.
		List<ItemStack> l = new ArrayList<ItemStack>();
		l.add(northPiller);
		l.add(eastPiller);
		l.add(southPiller);
		l.add(westPiller);
		this.pillerInputs = l;
	}
	

	public RunicRecipe(ItemStack center, List<ItemStack> otherPillerIn, ItemStack result){
		centerInput = center;
		this.result = result;
		this.pillerInputs = otherPillerIn;
	}
	
	public ItemStack getResult(){
			return this.result;
	}
	
	public ItemStack getCenter(){
		return centerInput;
	}
	
	public boolean matches(ItemStack center, List<ItemStack> test){
		if (ItemUtils.stackEqualsNonNBT(center, this.centerInput)){
			return ItemUtils.stackListsMatch(this.pillerInputs, test);
		}
		return false;
	}


	public int getResultMeta() {
		// TODO Auto-generated method stub
		return resultmeta;
	}
}

