package com.grimoire.main.common.recipe.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.IFluidHandler;

public class ItemUtils {
	public static ItemStack copyStackWithSize(@Nonnull ItemStack stack, int amount) {
		if (stack.isEmpty() || amount <= 0)
			return ItemStack.EMPTY;
		ItemStack s = stack.copy();
		s.setCount(amount);
		return s;
	}

	public static boolean stackListsMatch(List<ItemStack> list1, List<ItemStack> list2) {
		ArrayList<ItemStack> list1backup = new ArrayList<ItemStack>();
		ArrayList<ItemStack> list2backup = new ArrayList<ItemStack>();

		// list2 is inputed items from the pillers, remove empty itemstacks to
		// allow players to use recipes that have diffrent amounts of pillers
		list2.removeIf(obj -> obj.isEmpty());
		if (list1.size() == list2.size()) {
			for (int i = 0; i < list1.size(); i++) {
				boolean doContinue = true;
				for (int j = 0; j < list2.size() && doContinue; j++) {
					if (stackEqualsNonNBT(list1.get(i), list2.get(j))) {
						list1backup.add(list1.get(i).copy());
						list2backup.add(list2.get(j).copy());
						list1.remove(i);
						list2.remove(j);
						i = -1;
						j = -1;
						doContinue = false;
					}
				}
			}
			boolean doMatch = false;
			if (list1.size() == 0 && list2.size() == 0) {
				doMatch = true;
			}
			for (ItemStack i : list1backup) {
				list1.add(i);
			}
			for (ItemStack i : list2backup) {
				list2.add(i);
			}
			return doMatch;
		}
		return false;
	}

	public static FluidActionResult drainFluidFromItem(ItemStack stack, Fluid fluid, int mbAmount, boolean doDrain) {
		return drainFluidFromItem(stack, new FluidStack(fluid, mbAmount), doDrain);
	}

	public static FluidActionResult drainFluidFromItem(ItemStack stack, FluidStack fluidStack, boolean doDrain) {
		return FluidUtil.tryEmptyContainer(stack, FluidHandlerVoid.INSTANCE, fluidStack.getAmount(), null, doDrain);
	}

	public static boolean stackEqualsNonNBT(@Nonnull ItemStack stack, @Nonnull ItemStack other) {
		if (stack.isEmpty() && other.isEmpty())
			return true;
		if (stack.isEmpty() || other.isEmpty())
			return false;
		Item sItem = stack.getItem();
		Item oItem = other.getItem();
		return sItem.equals(other.getItem()) && (stack.getDamageValue() == other.getDamageValue());

	}

	public static void writeString(PacketBuffer buf, String toWrite) {
		byte[] str = toWrite.getBytes(Charset.forName("UTF-8"));
		buf.writeInt(str.length);
		buf.writeBytes(str);
	}

	public static void writeString(ByteBuf buf, String toWrite) {
		byte[] str = toWrite.getBytes(Charset.forName("UTF-8"));
		buf.writeInt(str.length);
		buf.writeBytes(str);
	}

	public static String readString(PacketBuffer buf) {
		int length = buf.readInt();
		byte[] strBytes = new byte[length];
		buf.readBytes(strBytes, 0, length);
		return new String(strBytes, Charset.forName("UTF-8"));
	}

	public static String readString(ByteBuf buf) {
		int length = buf.readInt();
		byte[] strBytes = new byte[length];
		buf.readBytes(strBytes, 0, length);
		return new String(strBytes, Charset.forName("UTF-8"));
	}

	public static void writeItemStack(ByteBuf byteBuf, @Nonnull ItemStack stack) {
		boolean defined = !stack.isEmpty();
		byteBuf.writeBoolean(defined);
		if (defined) {
			CompoundNBT tag = new CompoundNBT();
			stack.save(tag);
			writeNBTTag(byteBuf, tag);
		}
	}

	@Nonnull
	public static ItemStack readItemStack(ByteBuf byteBuf) {
		boolean defined = byteBuf.readBoolean();
		if (defined) {
			return ItemStack.of(readNBTTag(byteBuf));
		} else {
			return ItemStack.EMPTY;
		}
	}

	public static void writeFluidStack(ByteBuf byteBuf, @Nullable FluidStack stack) {
		boolean defined = stack != null;
		byteBuf.writeBoolean(defined);
		if (defined) {
			CompoundNBT tag = new CompoundNBT();
			stack.writeToNBT(tag);
			writeNBTTag(byteBuf, tag);
		}
	}

	@Nullable
	public static FluidStack readFluidStack(ByteBuf byteBuf) {
		boolean defined = byteBuf.readBoolean();
		if (defined) {
			return FluidStack.loadFluidStackFromNBT(readNBTTag(byteBuf));
		} else {
			return null;
		}
	}

	public static void writeNBTTag(ByteBuf byteBuf, @Nonnull CompoundNBT tag) {
		try (DataOutputStream dos = new DataOutputStream(new ByteBufOutputStream(byteBuf))) {
			CompressedStreamTools.write(tag, dos);
		} catch (Exception exc) {
		}
	}

	@Nonnull
	public static CompoundNBT readNBTTag(ByteBuf byteBuf) {
		try (DataInputStream dis = new DataInputStream(new ByteBufInputStream(byteBuf))) {
			return CompressedStreamTools.read(dis);
		} catch (Exception exc) {
		}
		throw new IllegalStateException("Could not load NBT Tag from incoming byte buffer!");
	}

	private static class FluidHandlerVoid implements IFluidHandler {

		private static FluidHandlerVoid INSTANCE = new FluidHandlerVoid();

		@Override
		public int getTanks() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public FluidStack getFluidInTank(int tank) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int getTankCapacity(int tank) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public boolean isFluidValid(int tank, FluidStack stack) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public int fill(FluidStack resource, FluidAction action) {
			// TODO Auto-generated method stub
			return resource.getAmount();
		}

		@Override
		public FluidStack drain(FluidStack resource, FluidAction action) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public FluidStack drain(int maxDrain, FluidAction action) {
			// TODO Auto-generated method stub
			return null;
		}
	}

}
