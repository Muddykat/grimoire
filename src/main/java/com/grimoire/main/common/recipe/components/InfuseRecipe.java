package com.grimoire.main.common.recipe.components;

import net.minecraft.item.ItemStack;

public class InfuseRecipe {
	public ItemStack input = null;
	public ItemStack output = null;
	public int validWorldID = 0;
	
	public InfuseRecipe(ItemStack input, ItemStack output, int worldID){
		this.input = input;
		this.output = output;
		this.validWorldID = worldID;
	}
	
	public ItemStack getInput(){
		return input;
	}
	
	public ItemStack getOutput(){
		return output;
	}
	
	public boolean validWorld(int id){
		if(id == validWorldID){
			return true;
		} else {
			return false;
		}
	}
}
