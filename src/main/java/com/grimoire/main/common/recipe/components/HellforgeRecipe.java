package com.grimoire.main.common.recipe.components;

import net.minecraft.item.ItemStack;

public class HellforgeRecipe {
	public ItemStack input = null;
	public ItemStack output = null;
	
	public HellforgeRecipe(ItemStack input, ItemStack output){
		this.input = input;
		this.output = output;
	}
	
	public ItemStack getInput(){
		return input;
	}
	
	public ItemStack getOutput(){
		return output;
	}
}
