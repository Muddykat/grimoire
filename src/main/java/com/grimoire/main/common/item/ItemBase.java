package com.grimoire.main.common.item;

import com.grimoire.main.client.menu.helper.GrimoireSubGroup;
import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.registry.ItemGroupRegistry;

import net.minecraft.item.Item;

public class ItemBase extends Item implements GrimoireSubGroup {

	protected ItemSubGroup subGroup;
	
	public ItemBase(Properties properties, String registryName, ItemSubGroup subGroup) {
		super(properties);
		this.setRegistryName("grimoire", registryName);
		properties.tab(ItemGroupRegistry.GRIMOIRE);
		this.subGroup = subGroup;
	}

	@Override
	public ItemSubGroup getSubGroup() {
		// TODO Auto-generated method stub
		return subGroup;
	} 

}
