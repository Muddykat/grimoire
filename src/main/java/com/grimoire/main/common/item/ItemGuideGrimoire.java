package com.grimoire.main.common.item;

import com.grimoire.main.client.gui.GrimoireScreen;
import com.grimoire.main.client.menu.GrimoireItemGroup;
import com.grimoire.main.client.menu.helper.ItemSubGroup;
import com.grimoire.main.common.gui.GuiWrapper;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class ItemGuideGrimoire extends ItemBase {

	public ItemGuideGrimoire(Properties properties, String registryName, ItemSubGroup subGroup) {
		super(properties, registryName, subGroup);
		properties.stacksTo(1);
	}


	@Override
	public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
		Minecraft.getInstance().setScreen(new GrimoireScreen(new StringTextComponent("Grimoire")));
		return super.use(world, player, hand);
	}
}