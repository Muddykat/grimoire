package com.grimoire.main.common.structure.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;

public class BlockArray {

    protected static final Random STATIC_RAND = new Random();

    protected Map<BlockPos, TileEntityCallback> tileCallbacks = new HashMap<>();
    protected Map<BlockPos, BlockInformation> pattern = new HashMap<>();
    private Vector3i min = new Vector3i(0, 0, 0), max = new Vector3i(0, 0, 0), size = new Vector3i(0, 0, 0);

    public void addBlock(int x, int y, int z, @Nonnull BlockState state) {
        addBlock(new BlockPos(x, y, z), state);
    }

    public void addBlock(BlockPos offset, @Nonnull BlockState state) {
        Block b = state.getBlock();
        pattern.put(offset, new BlockInformation(b, state));
        updateSize(offset);
    }

    public void addBlock(int x, int y, int z, @Nonnull BlockState state, BlockStateCheck match) {
        addBlock(new BlockPos(x, y, z), state, match);
    }

    public void addBlock(BlockPos offset, @Nonnull BlockState state, BlockStateCheck match) {
        Block b = state.getBlock();
        pattern.put(offset, new BlockInformation(b, state, match));
        updateSize(offset);
    }

    public void addAll(BlockArray other) {
        addAll(other, null);
    }

    public void addAll(BlockArray other, @Nullable Function<BlockPos, BlockPos> positionTransform) {
        for (Map.Entry<BlockPos, BlockInformation> patternEntry : other.pattern.entrySet()) {
            BlockPos to = patternEntry.getKey();
            if(positionTransform != null) {
                to = positionTransform.apply(to);
            }
            pattern.put(to, patternEntry.getValue());
            updateSize(to);
        }
        for (Map.Entry<BlockPos, TileEntityCallback> patternEntry : other.tileCallbacks.entrySet()) {
            BlockPos to = patternEntry.getKey();
            if(positionTransform != null) {
                to = positionTransform.apply(to);
            }
            tileCallbacks.put(to, patternEntry.getValue());
        }
    }

    public void addTileCallback(BlockPos pos, TileEntityCallback callback) {
        tileCallbacks.put(pos, callback);
    }

    public boolean hasBlockAt(BlockPos pos) {
        return pattern.containsKey(pos);
    }

    public boolean isEmpty() {
        return pattern.isEmpty();
    }

    public Vector3i getMax() {
        return max;
    }

    public Vector3i getMin() {
        return min;
    }

    public Vector3i getSize() {
        return size;
    }

    private void updateSize(BlockPos addedPos) {
        if(addedPos.getX() < min.getX()) {
            min = new Vector3i(addedPos.getX(), min.getY(), min.getZ());
        }
        if(addedPos.getX() > max.getX()) {
            max = new Vector3i(addedPos.getX(), max.getY(), max.getZ());
        }
        if(addedPos.getY() < min.getY()) {
            min = new Vector3i(min.getX(), addedPos.getY(), min.getZ());
        }
        if(addedPos.getY() > max.getY()) {
            max = new Vector3i(max.getX(), addedPos.getY(), max.getZ());
        }
        if(addedPos.getZ() < min.getZ()) {
            min = new Vector3i(min.getX(), min.getY(), addedPos.getZ());
        }
        if(addedPos.getZ() > max.getZ()) {
            max = new Vector3i(max.getX(), max.getY(), addedPos.getZ());
        }
        size = new Vector3i(max.getX() - min.getX() + 1, max.getY() - min.getY() + 1, max.getZ() - min.getZ() + 1);
    }

    public Map<BlockPos, BlockInformation> getPattern() {
        return pattern;
    }

    public int getBlockSize() {
        return this.getPattern().size();
    }

    public Map<BlockPos, TileEntityCallback> getTileCallbacks() {
        return tileCallbacks;
    }

    public void addBlockCube(@Nonnull BlockState state, int ox, int oy, int oz, int tx, int ty, int tz) {
        addBlockCube(state, new BlockStateCheck.Meta(state.getBlock(), Block.getId(state)), ox, oy, oz, tx, ty, tz);
    }

    public void addBlockCube(@Nonnull BlockState state, BlockStateCheck match, int ox, int oy, int oz, int tx, int ty, int tz) {
        int lx, ly, lz;
        int hx, hy, hz;
        if(ox < tx) {
            lx = ox;
            hx = tx;
        } else {
            lx = tx;
            hx = ox;
        }
        if(oy < ty) {
            ly = oy;
            hy = ty;
        } else {
            ly = ty;
            hy = oy;
        }
        if(oz < tz) {
            lz = oz;
            hz = tz;
        } else {
            lz = tz;
            hz = oz;
        }

        for (int xx = lx; xx <= hx; xx++) {
            for (int zz = lz; zz <= hz; zz++) {
                for (int yy = ly; yy <= hy; yy++) {
                    addBlock(new BlockPos(xx, yy, zz), state, match);
                }
            }
        }
    }

    public Map<BlockPos, BlockState> placeInWorld(World world, BlockPos center) {
        Map<BlockPos, BlockState> result = new HashMap<>();
        for (Map.Entry<BlockPos, BlockInformation> entry : pattern.entrySet()) {
            BlockInformation info = entry.getValue();
            BlockPos at = center.offset(entry.getKey());
            BlockState state = info.state;
            if (!world.setBlock(at, state, 3)) {
                continue;
            }
            result.put(at, state);

            if(BlockUtils.isFluidBlock(state)) {
                world.neighborChanged(at, state.getBlock(), at);
            }

            TileEntity placed = world.getBlockEntity(at);
            if(tileCallbacks.containsKey(entry.getKey())) {
                TileEntityCallback callback = tileCallbacks.get(entry.getKey());
                if(callback.isApplicable(placed)) {
                    callback.onPlace(world, at, placed);
                }
            }
        }
        return result;
    }

    public List<ItemStack> getAsDescriptiveStacks() {
        List<ItemStack> out = new LinkedList<>();
        for (BlockInformation info : pattern.values()) {
            int meta = info.metadata;
            ItemStack s;
            Item i = info.type.asItem();
            if(i == Items.AIR) continue;
            s = new ItemStack(i, 1);
            
            if(!s.isEmpty()) {
                boolean found = false;
                for (ItemStack stack : out) {
                    if(stack.getItem().getRegistryName().equals(s.getItem().getRegistryName()) && stack.getDamageValue() == s.getDamageValue()) {
                        stack.setCount(stack.getCount() + 1);
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    out.add(s);
                }
            }
        }
        return out;
    }

    public static interface TileEntityCallback {

        public boolean isApplicable(TileEntity te);

        public void onPlace(IBlockReader access, BlockPos at, TileEntity te);

    }

    public static class BlockInformation {

        public final Block type;
        public final BlockState state;
        public final int metadata;
        public final BlockStateCheck matcher;

        protected BlockInformation(Block type, BlockState state) {
            this(type, state, new BlockStateCheck.Meta(type, Block.getId(state)));
        }

        protected BlockInformation(Block type, BlockState state, BlockStateCheck matcher) {
            this.type = type;
            this.state = state;
            this.metadata = Block.getId(state);
            this.matcher = matcher;
        }

    }
}