package com.grimoire.main.common.structure.util;

import java.util.Collection;
import java.util.function.Function;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.Fluids;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunk;
import net.minecraftforge.common.Tags.Blocks;

public class BlockUtils {
	@Nullable
	public static <T> T getTileAt(IBlockReader world, BlockPos pos, Class<T> tileClass, boolean forceChunkLoad) {
		if (world == null || pos == null)
			return null; // Duh.
		if (world instanceof World) {
			if (!((World) world).isLoaded(pos) && !forceChunkLoad)
				return null;
		}
		TileEntity te = world.getBlockEntity(pos);
		if (te == null)
			return null;
		if (tileClass.isInstance(te))
			return (T) te;
		return null;
	}

	public static boolean isFluidBlock(BlockState state) {
		return false;
	}
	
	@Nullable
	public static <T> T iterativeSearch(Collection<T> collection, Function<T, Boolean> matchingFct) {
		for (T element : collection) {
			if (matchingFct.apply(element)) {
				return element;
			}
		}
		return null;
	}
	
	public static <T> boolean contains(Collection<T> collection, Function<T, Boolean> matchingFct) {
		return iterativeSearch(collection, matchingFct) != null;
	}

	public static void syncTE(TileEntity tile) {
		BlockState state = tile.getLevel().getBlockState(tile.getBlockPos());
		tile.getLevel().markAndNotifyBlock(tile.getBlockPos(), tile.getLevel().getChunkAt(tile.getBlockPos()), state, state, 3, 3);
	}
	
	
}
