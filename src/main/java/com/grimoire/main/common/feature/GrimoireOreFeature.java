package com.grimoire.main.common.feature;

import com.grimoire.main.lib.MathLib;
import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.template.RuleTest;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.placement.TopSolidRangeConfig;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;

import java.util.Objects;

public class GrimoireOreFeature extends GrimoireFeature<GrimoireOreFeature> {
    private final Block ore;

    private final RuleTest target;

    private final int veinRarity;

    private final int veinSize;

    private final int veinMinHeight;

    private final int veinMaxHeight;

    public GrimoireOreFeature(ResourceLocation id, Biome.Category[] biomes, Block ore, RuleTest target,
                              int rarity, int size, int minHeight, int maxHeight) {
        super(id, biomes);

        this.ore = Objects.requireNonNull(ore);
        this.target = Objects.requireNonNull(target);

        this.veinRarity = MathLib.within(rarity, 1, 64);
        this.veinSize = MathLib.within(size, 1, 64);
        this.veinMinHeight = MathLib.within(minHeight, 1, 254);
        this.veinMaxHeight = MathLib.within(maxHeight, minHeight + 1, 255);
    }

    @Override
    protected ConfiguredFeature<?, ?> constructFeature() {
        return Feature.ORE.configured(
                new OreFeatureConfig(target, ore.defaultBlockState(), veinSize)
        ).range(veinMaxHeight).squared().chance(veinRarity);
    }

    @Override
    protected void onConfigureFeature(BiomeGenerationSettingsBuilder builder) throws Exception {
        if(ore.getRegistryName() == null) throw new Exception("Ore registry name is null");

        if(getFeature() == null) throw new Exception("Ore Feature was not constructed correctly!");
        else builder.addFeature(GenerationStage.Decoration.UNDERGROUND_ORES, getFeature());
    }
}
