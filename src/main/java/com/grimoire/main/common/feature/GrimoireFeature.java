package com.grimoire.main.common.feature;

import com.grimoire.main.Grimoire;
import com.grimoire.main.registry.FeatureRegistry;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public abstract class GrimoireFeature<T extends GrimoireFeature<T>> {
    private static final Logger LOG = Grimoire.getNewLogger();

    private final ResourceLocation id;

    private final List<Biome.Category> biomes;

    private ConfiguredFeature<?, ?> feature;

    protected GrimoireFeature(ResourceLocation id, Biome.Category[] biomes){
        this.id = Objects.requireNonNull(id);
        this.biomes = Arrays.asList(biomes);
    }

    protected abstract ConfiguredFeature<?, ?> constructFeature();

    protected abstract void onConfigureFeature(BiomeGenerationSettingsBuilder builder) throws Exception;

    public final void configure(BiomeGenerationSettingsBuilder builder, Biome.Category biomeCategory) {
        if(!biomes.contains(biomeCategory))
            return;

        try{
            LOG.debug("Configuring Grimoire feature: '" + id.toString() + "' for biome: '" + biomeCategory.getName() + "'...");
            onConfigureFeature(builder);
        } catch (Exception e) {
            LOG.error("Failed to configure feature: " + id.toString(), e);
        }
    }

    public ResourceLocation getID() {
        return this.id;
    }

    public ConfiguredFeature<?, ?> getFeature() {
        return feature == null ? feature = constructFeature() : feature;
    }

    public GrimoireFeature<T> register(){
        return FeatureRegistry.addFeature(this);
    }

}
