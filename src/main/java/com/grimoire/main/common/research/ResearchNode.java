package com.grimoire.main.common.research;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class ResearchNode {

	private ArrayList<ResearchNode> parents = new ArrayList<ResearchNode>();
	private ArrayList<ResearchNode> children = new ArrayList<ResearchNode>();
	private Page researchPage = null;
	private Page unlockedPage = null;	
	private String name = "UNKNOWN";
	private ItemStack nodeIcon = new ItemStack(Items.GOLDEN_APPLE);
	private int posX = 0, posY = 0;
	private boolean isUnlocked = false;
	private boolean isCompleted = false;
	private String categoryName;
	private int categoryID = -1;
	private int id = -1;
	private int flipedPosition = 0;
	private ArrayList<ItemStack> nodeIconList = new ArrayList<ItemStack>();
	private int currentIconDisplay = 0;
	private int timer = 0;
	
	public ResearchNode setFlipedPosition(int flip){
		this.flipedPosition = flip;
		return this;
	}
	
	public ResearchNode(Page refrencePage) {
		this.researchPage = refrencePage;
		this.unlockedPage = null;
	}

	public ResearchNode setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
		return this;
	}

	public ResearchNode setCompletedPage(Page unlocked){
		this.unlockedPage = unlocked;
		return this;
	}
	
	public Page getCompletedPage(){
		return this.unlockedPage;
	}
	
	public int getPosX() {
		return this.posX;
	}

	public int getPosY() {
		return this.posY;
	}

	public ResearchNode setName(String name) {
		this.name = name;
		return this;
	}

	public ResearchNode(Page refrencePage, ResearchNode parent) {
		this.researchPage = refrencePage;
		parents.add(parent);
	}

	public ResearchNode setCategoryName(String name) {
		this.categoryName = name;
		return this;
	}

	public ResearchNode setCategoryID(int catID) {
		this.categoryID = catID;
		return this;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public ResearchNode addChild(ResearchNode child) {
		child.addParentOnly(this);
		child.setCategoryID(this.categoryID);
		children.add(child);
		return this;
	}

	public ResearchNode addParent(ResearchNode parent) {
		parents.add(parent);
		parent.addChildOnly(this);
		return this;
	}

	public ResearchNode addChildOnly(ResearchNode child) {
		child.setCategoryID(this.categoryID);
		children.add(child);
		return this;
	}

	public ResearchNode addParentOnly(ResearchNode parent) {
		parents.add(parent);
		return this;
	}

	public ArrayList<ResearchNode> getParents() {
		return parents;
	}

	public ArrayList<ResearchNode> getChildren() {
		return children;
	}

	public ResearchNode setIcon(ItemStack item) {
		this.nodeIcon = item.copy();
		return this;
	}
	
	public ResearchNode setIcon(ArrayList<ItemStack> itemList) {
		this.nodeIconList  = itemList;
		return this;
	}

	public ItemStack getIcon() {
		Random rand = new Random();
		if(this.nodeIconList.isEmpty()){
			return this.nodeIcon;
		} else {
			if(timer >= 150){
				if(currentIconDisplay < nodeIconList.size() - 1){
					currentIconDisplay++;
				} else {
					currentIconDisplay = 0;
				}
				timer = 0;
			} else {
				timer++;
			}
			return this.nodeIconList.get(currentIconDisplay);
		}
	}

	// method is unlikly to be used, but I might need it at some point.
	public boolean tryUnlock() {
		for (ResearchNode node : parents) {
			if (!node.isUnlocked) {
				this.isUnlocked = false;
				return false;
			}
		}

		this.isUnlocked = true;
		return true;
	}

	public boolean tryUnlock(boolean yes) {
		if (yes) {
			return tryUnlock();
		} else {
			return false;
		}
	}

	public boolean tryComplete(boolean yes) {
		if (yes) {
			return tryComplete();
		} else {
			return false;
		}
	}

	public boolean tryComplete() {
		boolean canComplete = true;
		for (ResearchNode node : parents) {
			if (!node.isCompleted) {
				this.isCompleted = false;
				canComplete = false;
			}
		}

		if(canComplete){
			for (ResearchNode b : getChildren()) {
				b.tryUnlock();
			}
			if(this.unlockedPage != null){
				this.unlockedPage.setLinkedPage(this.researchPage);
			}
			this.isCompleted = true;
			return true;
		} else {
			return false;
		}
	}

	public boolean canAccess() {
		for (ResearchNode node : parents) {
			if (!node.isUnlocked || !node.isCompleted) {
				return false;
			}
		}

		return true;
	}

	public ResearchNode setUnlocked(boolean isUnlocked) {
		this.isUnlocked = isUnlocked;
		return this;
	}

	public boolean isUnlocked() {
		return this.isUnlocked;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	public Page getPage() {
		return researchPage;
	}

	public ResearchNode setCompleted(boolean completed) {
		this.isCompleted = completed;
		return this;
	}

	public boolean getCompleted() {
		// TODO Auto-generated method stub
		return isCompleted;
	}

	public String getCategoryName() {
		// TODO Auto-generated method stub
		return this.categoryName;
	}

	public ResearchNode setID(int i) {
		// TODO Auto-generated method stub
		this.id = i;
		return this;
	}

	public int getID() {
		return this.id;
	}

	public void clearCompletion(boolean isRoot) {
		setCompleted(false);
		if (isRoot) {
			setUnlocked(true);
			setCompleted(false);
		} else {
			setUnlocked(false);
			setCompleted(false);
		}
		
		for (ResearchNode child : getChildren()) {
			child.clearCompletion(false);
		}
	}

	public int getIsFliped() {
		return flipedPosition;
	}
}
