package com.grimoire.main.common.research;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.grimoire.main.common.recipe.components.RunicRecipe;
import com.grimoire.main.common.structure.util.BlockArray;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.RecipeManager;
import net.minecraft.nbt.CompoundNBT;

public class Page {
	public static enum EnumPageType {
		TEXT, ITEM_DISPLAY, STRUCTURE_DISPLAY, FURNACE, CRAFTING, RESEARCH, RITUAL
	}

	static final Color runology = new Color(40, 67, 204);
	static final Color demonology = new Color(67, 44, 176);
	static final Color sorcery = new Color(93, 25, 127);
	static final RecipeManager rm = new RecipeManager();
	private ArrayList<RunicRecipe> ritualRecipeList = new ArrayList<RunicRecipe>();
	
	private ItemStack[] craftingInputs; 
	private ItemStack craftingOutput;

	private ItemStack furnaceInput;
	private ItemStack furnaceOutput;

	private ItemStack displayStack;
	private BlockArray displayBlocks;

	private EnumPageType type = EnumPageType.TEXT;
	private String name = "";
	private String categoryName = "";
	private int catID = 0;
	
	private Page linkedPage = null;
	private Page parentLink = null;
	
	private ItemStack researchStack;
	
	public Page(String pageName, String categoryName, int categoryID) {
		this.name = pageName;
		this.categoryName = categoryName;
		this.catID = categoryID;
	}

	public Page setText() {
		this.type = EnumPageType.TEXT;
		return this;
	}
	
	public Page setResearchPage(ItemStack input){
		this.type = EnumPageType.RESEARCH;
		this.researchStack = input;
		return this;
	}

	public Page setItemDisplay(ItemStack stack) {
		this.type = EnumPageType.ITEM_DISPLAY;
		this.displayStack = stack;
		return this;
	}

	public Page setStructureDisplay(BlockArray array) {
		this.displayBlocks = array;
		this.type = EnumPageType.STRUCTURE_DISPLAY;
		return this;
	}
	
	public Page addRitualRecipe(RunicRecipe recipe){
		this.type = EnumPageType.RITUAL;
		ritualRecipeList.add(recipe);		
		return this;
	}

	public String getName() {
		return this.name;
	}
	
	public Page setLinkedPage(Page linkedPage){
		if(linkedPage != null){
			linkedPage.setParentLink(this);
		}
		this.linkedPage = linkedPage;
		return this;
	}

	private void setParentLink(Page page) {
		this.parentLink = page;
	}	

	public EnumPageType getType() {
		// TODO Auto-generated method stub
		return type;
	}

	public String getCategoryName() {
		// TODO Auto-generated method stub
		return categoryName;
	}

	public ItemStack getDisplayItem() {
		// TODO Auto-generated method stub
		return displayStack;
	}

	public BlockArray getDisplayStructure() {
		// TODO Auto-generated method stub
		return displayBlocks;
	}

	public Page setFurnaceRecipe(ItemStack input, ItemStack output) {
		this.type = EnumPageType.FURNACE;
		this.furnaceInput = input;
		this.furnaceOutput = output;
		return this;
	}

	public Page setCraftingRecipe(ItemStack[] inputs, Item output) {
		this.type = EnumPageType.CRAFTING;
		this.craftingInputs = inputs;
		this.craftingOutput = new ItemStack(output);

		return this;
	}

	public Page setCraftingRecipe(ItemStack[] inputs, Block output) {
		this.type = EnumPageType.CRAFTING;
		this.craftingInputs = inputs;
		this.craftingOutput = new ItemStack(output);
		return this;
	}

	public Page setCraftingRecipe(ItemStack stack) {
		this.type = EnumPageType.CRAFTING;
		this.craftingInputs = new ItemStack[9];
		for (int i = 0; i < 9; i++) {
			craftingInputs[i] = ItemStack.EMPTY;
		}

		this.craftingOutput = stack;
		
		
		IRecipe recipe = null;
		for (IRecipe r : rm.getRecipes()) {
			if (r.getResultItem().getItem() == craftingOutput.getItem()
					&& r.getResultItem().getDamageValue() == craftingOutput.getDamageValue()) {
				recipe = r;
			}
		}
		if (recipe != null) {
			List<Ingredient> stacks = recipe.getIngredients();
			if (recipe.canCraftInDimensions(2, 2)) {
				for (int i = 0; i < stacks.size(); i++) {
					craftingInputs[i + (int) (i / 2)] = getStack(stacks.get(i));
				}
			} else
				for (int i = 0; i < stacks.size(); i++) {
					craftingInputs[i] = getStack(stacks.get(i));
				}
		}
		return this;
	}

	public ItemStack getStack(Ingredient i) {
		if (i == Ingredient.EMPTY || i.getItems() == null || i.getItems().length == 0) {
			return ItemStack.EMPTY;
		} else {
			int meta = i.getItems()[0].getDamageValue();
			return new ItemStack(i.getItems()[0].getItem(), 1);
		}
	}

	public ItemStack getCraftingOutput() {
		return this.craftingOutput;
	}

	public ItemStack[] getCraftingInputs() {
		// TODO Auto-generated method stub
		return this.craftingInputs;
	}

	public static String getDefaultSaveKey() {
		return "pageName";
	}

	public void writeToNBT(CompoundNBT compound) {
		writeToNBT(compound, getDefaultSaveKey());
	}

	public void writeToNBT(CompoundNBT compound, String key) {
		compound.putString(key, getName()); 
	}
	public boolean hasLinkedPage() {
		if(this.linkedPage != null){
			return true; 
		} else {
			return false;
		}
	}
	
	public Page getLinkedPage(){
		return this.linkedPage;
	}
	
	public Page getParentPage(){
		return this.parentLink;
	}

	public boolean hasLinkedParent() {
		if(parentLink != null){
			return true;
		} else {
			return false;
		}
	}
	
	public Page setCategoryID(int id){
		this.catID = id;
		return this;
	}

	public int getCategoryID() {
		return catID;
	}

	public ItemStack getFurnaceInput() {
		return this.furnaceInput;
	}
	
	public ItemStack getFurnaceOutput(){
		return this.furnaceOutput;
	}

	public ItemStack getResearchInput() {
		// TODO Auto-generated method stub
		return this.researchStack;
	}

	public ItemStack[] getRitualInputs(int index) {
		// TODO Auto-generated method stub
		if(ritualRecipeList.size() - 1 < index) {
			return this.ritualRecipeList.get(this.ritualRecipeList.size() -1).pillerInputs.toArray(new ItemStack[this.ritualRecipeList.get(this.ritualRecipeList.size() -1).pillerInputs.size()]);
		} else {
			return this.ritualRecipeList.get(index).pillerInputs.toArray(new ItemStack[this.ritualRecipeList.get(index).pillerInputs.size()]);
		}
	}

	public ItemStack getRitualCatalist(int index) {
		// TODO Auto-generated method stub
		if(ritualRecipeList.size() - 1 < index) {
			return this.ritualRecipeList.get(ritualRecipeList.size() -1).centerInput;
		} else {
			return this.ritualRecipeList.get(index).centerInput;
		}
	}

	public ItemStack getRitualOutput(int index) {
		// TODO Auto-generated method stub
		if(ritualRecipeList.size() - 1 < index) {
			return this.ritualRecipeList.get(ritualRecipeList.size() -1).result;
		} else {
			return this.ritualRecipeList.get(index).result;
		}
	}

	public int getRitualSize() {
		return ritualRecipeList.size();
	}

}

