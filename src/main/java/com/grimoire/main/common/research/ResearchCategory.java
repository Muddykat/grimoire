package com.grimoire.main.common.research;

import java.util.ArrayList;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class ResearchCategory {
	private ResearchNode ROOT;
	private String name;
	private ItemStack categoryIcon;

	public ResearchCategory(ResearchNode ROOT, String name) {
		this.ROOT = ROOT;
		this.name = name;
		this.categoryIcon = new ItemStack(Items.GOLDEN_APPLE);
	}

	public String getName() {
		return this.name;
	}

	public ResearchNode getRoot() {
		return this.ROOT;
	}

	public ItemStack getItemIcon() {
		return this.categoryIcon;
	}

	public ResearchNode getResearchByID(int resOrdinal) {
		ResearchNode selectedNode = getRoot();

		childList.clear();
		
		gatherAllChildren(getRoot());
		
		if(!childList.isEmpty()){
			for(ResearchNode node : childList){
				if(node.getID() == resOrdinal){
					selectedNode = node;
				}
			}
		}
		return selectedNode;
	}



	private ArrayList<ResearchNode> childList = new ArrayList<ResearchNode>();

	public ResearchNode getResearchByPageName(String pageName) {
		ResearchNode selectedNode = getRoot();
		childList.clear();
		gatherAllChildren(getRoot());
		
		for(ResearchNode node : childList){
			if(node.getPage().getName().equals(pageName)){
				selectedNode = node;
			}
		}
		
		return selectedNode;
	}

	private void gatherAllChildren(ResearchNode root) {
		if(root != null){
			childList.add(root);			
			for (ResearchNode child : root.getChildren()) {
				gatherAllChildren(child);
			}
		}
	}

}