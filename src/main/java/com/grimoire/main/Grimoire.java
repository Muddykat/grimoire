package com.grimoire.main;

import com.grimoire.main.proxy.ClientProxy;
import com.grimoire.main.proxy.Proxy;
import com.grimoire.main.proxy.ServerProxy;
import com.grimoire.main.registry.FeatureRegistry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.Feature;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.event.lifecycle.*;
import net.minecraftforge.fml.event.server.FMLServerStoppedEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.grimoire.main.client.menu.handler.CreativeMenuHandler;
import com.grimoire.main.lib.GrimoireLib;
import com.grimoire.main.registry.BlockRegistry;
import com.grimoire.main.registry.ItemRegistry;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.event.server.FMLServerStoppingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.util.StackLocatorUtil;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(GrimoireLib.MODID)
public class Grimoire  
{
    // Directly reference a log4j logger.
    private static final Logger LOGGER = getNewLogger();

    private static final Proxy proxy = DistExecutor.safeRunForDist(() -> ClientProxy::new, () -> ServerProxy::new);

    public Grimoire() {
        // Register the setup method for modloading
        IEventBus forgeBus = MinecraftForge.EVENT_BUS;
        IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();

        //Common Listeners
        modBus.addListener(this::setup);
        modBus.addListener(this::onFinishSetup);


        //Client Listeners
        modBus.addListener(this::doClientStuff);

        //Mod Communication Listeners
        modBus.addListener(this::enqueueIMC);
        modBus.addListener(this::processIMC);

        //Register ourselves for server and other game events we are interested in
        forgeBus.register(this);
    }

    private void setup(final FMLCommonSetupEvent event)
    {
        LOGGER.info(String.format("Beginning setup for Grimoire V'%s'", GrimoireLib.VERSION));
        proxy.onSetup(event);
    }

    private void onFinishSetup(final FMLLoadCompleteEvent event) {
        LOGGER.info("Finishing Grimoire setup...");
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        LOGGER.info("Starting Client Handlers!");
        if(proxy instanceof ClientProxy) { proxy.onClientSetup(event); }
    }

    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        proxy.onEnqueueModComs(event);
    }

    private void processIMC(final InterModProcessEvent event)
    {
        proxy.onProcessModComs(event);
    }

    @SubscribeEvent
    public static void onServerStarting(FMLServerStartingEvent event) {
        proxy.onServerStarting(event);
    }

    @SubscribeEvent
    public static void onServerStopped(FMLServerStoppedEvent event) {
        if(!(proxy instanceof ClientProxy)) proxy.onServerStopped(event);
    }

    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
    	@SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> event) {
           BlockRegistry.parseBlockRegistry(event);
    	}
    	
    	@SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
           BlockRegistry.parseBlockItemRegistry(event);
           ItemRegistry.parseItemRegistry(event);
        }
    }

    public static Logger getNewLogger() {
        return LogManager.getLogger(StackLocatorUtil.getCallerClass(2));
    }
}

