package com.grimoire.main.lib;

public class MathLib {
    public static double within(double in, double min, double max){
        if(in > max) return max;
        else return Math.max(in, min);
    }

    public static int within(int in, int min, int max){
        if(in > max) return max;
        else return Math.max(in, min);
    }

    public static long within(long in, long min, long max){
        if(in > max) return max;
        else return Math.max(in, min);
    }
}
